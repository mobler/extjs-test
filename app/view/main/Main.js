Ext.define('App.view.main.Main',{
    extend: 'Ext.tab.Panel' ,
    
    requires: [ 
        'App.viewModel.TaskViewModel',
        'App.view.task.TaskGrid'
    ],

    viewModel: {
        type: 'task'
    },
    controller: 'task',


    items: [{
        title: 'Tasks',
        margin: 5,
        
        items: [{
            xtype: 'taskControllButtons'
        },
        {
            xtype: 'taskFilterFiled',
            margin: '5 0 0 0',
        },
        {
            xtype: 'container',
            layout: 'column',
            defaults: {
                height: 600
            },
            items: [
                {
                    xtype: 'taskGrid', 
                    columnWidth: 1,
                    margin: '5 0 0 0'
                },
                {
                    xtype: 'taskDetails',
                    margin: '5 0 0 5',
                    width: 350
                }
            ]
        },
        { 
            xtype: 'panel',
            layout: {
                type: 'vbox',
                align: 'right' 
            },
            margin: '5, 0, 0, 0',  
            items: [ 
               {
                   xtype: 'container',
                   items: [
                    {
                        xtype: 'button',
                        text: 'Sumbit' ,
                        margin: '0, 5, 0, 0',
                        width: 100, 
                         
                        listeners: {
                            click: 'onSumbitClick'
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Cancel' ,
                        margin: '0, 5, 0, 0',
                        
                        width: 100,
                        listeners: {
                            click: 'onCancelClick'
                        }
                    }
                   ]
               }]
        }
    ]
    }]
})