Ext.define('App.view.task.TaskControllButtons',{
    extend: 'Ext.container.Container',
    xtype: 'taskControllButtons',

    items: [{
        xtype: 'container', 
        items : [
            {
                xtype: 'button',
                text: 'Add' ,
                iconCls: 'pictos pictos-add2',
                margin: '0, 5, 0, 0',
           
                listeners: {
                    click: 'onAddClick'
                }
            },          
            {
                xtype: 'button',
                text: 'Remove',
                iconCls: 'pictos pictos-delete_black1',
                margin: '0, 15, 0, 0',
                bind: 
                {
                    disabled: '{!countSelected}'
                },
                listeners: {
                    click: 'onRemoveClick'
                }
            },
            {
                xtype: 'button',
                text: 'Run',
                iconCls: 'pictos pictos-play',
                margin: '0, 5, 0, 0',
                bind: 
                {
                    disabled: '{!isCanRunSelection}'
                },
                listeners: {
                    click: 'onRunClick'
                }
            },
            {
                xtype: 'button',
                text: 'Stop',
                iconCls: 'pictos pictos-stop',
                margin: '0, 5, 0, 0',
                bind: 
                {
                    disabled: '{!isCanStopSelection}'
                },
                listeners: {
                    click: 'onStopClick'
                }
            }
        ]
    }]
});