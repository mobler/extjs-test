Ext.define('App.view.task.TaskFilterFiled',{
    extend: 'Ext.container.Container',
    xtype: 'taskFilterFiled',

    items: [{
        xtype: 'container',  
        style: {borderColor:'#000000', borderStyle:'solid', borderWidth:'1px'},
        layout: {
            type: 'hbox',
            align: 'center' 
        },
        height: 45,
        items: [
            {  

                xtype: 'textfield',
                name: 'name',
                fieldLabel: 'Name:',
                bind:{
                    value: '{taskNameForFilter}'
                },
               // allowBlank: false,  // requires a non-empty value  
                margin: '0 0 0 10',
            }
        ]
    }]
});