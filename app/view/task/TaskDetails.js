Ext.define('App.view.task.TaskDetails',{
    extend: 'Ext.form.Panel',
    xtype: 'taskDetails',

    title: 'Details',
    bodyPadding: 5,
    style: {borderColor:'#000000', borderStyle:'solid', borderWidth:'1px'}, 
    disabled: false,
    bind: 
    {
        disabled: '{!isSingleSelection}'
    },
   
    defaultType: 'textfield',
    items: [
        {
            xtype: 'fieldset',
            title: 'General options',
            defaultType: 'textfield',
            defaults: {
                anchor: '100%',
                labelWidth: 100
            },
        
            items: [
                {
                    xtype: 'displayfield',
                    fieldLabel: 'Id',
                    name: 'id', 
                    bind: '{taskGrid.selection.id}' 
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Name:',
                    name: 'name', 
                    validator: function(val) {
                        val = val.trim();
                        return (val.length > 0 && val.length <= 255) ? true : "This field may not be empty and length range 1 to 255";
                    },
                    bind: '{taskGrid.selection.name}' 
                },
                {
                    xtype: 'datefield',
                    fieldLabel: 'Start data:',
                    name: 'startData', 
                    format:'d-m-Y h:m:s',
                    bind: { 
                        value: '{taskGrid.selection.startData}'
                    } 
                    
                },
                {
                    xtype: 'datefield',
                    fieldLabel: 'End data:',
                    name: 'endData',
                    format:'d-m-Y h:m:s', 
                    bind: {
                        minValue: '{taskGrid.selection.startData}',
                        value: '{taskGrid.selection.endData}'
                    }
                }
            ]
        }
    ]
});

