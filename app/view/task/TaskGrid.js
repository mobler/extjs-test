 

Ext.define('App.view.task.TaskGrid',{
    extend: 'Ext.grid.Panel',
    xtype: 'taskGrid',
    
    requires: [ 
        'App.store.Task' 
    ],

    reference: 'taskGrid',
  
    style: {borderColor:'#000000', borderStyle:'solid', borderWidth:'1px'},

    selModel: {
        selType: 'checkboxmodel',
        showHeaderCheckbox: true
    },

    bind:{
        store: '{task}'  
    },
    
    plugins: 'gridfilters',

    columns: [ 
        { text: 'Id', dataIndex: 'id', width:50},
        { text: 'Name', dataIndex: 'name',  flex: 1},
        { text: 'Start Data', dataIndex: 'startData', xtype: 'datecolumn',   format:'d-m-Y h:m:s', flex: 1},
        
        { text: 'End Data', dataIndex: 'endData', xtype: 'datecolumn',   format:'d-m-Y h:m:s', flex: 1},
        { text: 'Status', dataIndex: 'status' , flex: 1}
    ],

    listeners: {
        selectionchange: 'onSelectionchange'
    }
 
 
}); 