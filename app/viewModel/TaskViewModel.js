Ext.define('App.viewModel.TaskViewModel',{
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.task',

    data: {
        selections: [],
        countSelected: 0,
        taskNameForFilter: '',
    },

    stores: {
        task: {
            type: 'task',
            session: true,
            filters: [{
                property: 'name',
                value: '{taskNameForFilter}',
                anyMatch: true,
            }]
        }
    },
    
    formulas: {
        
        isCanStopSelection: function(get)
        {
            let selections = get('selections');
          
            for(let i=0; i<selections.length; i++)
            {
                if(selections[i].get('status') === 'RUNNING')
                {
                    return true;
                }
            }

            return false;
        },  
        isCanRunSelection: function(get)
        {
            let selections = get('selections');
            
            for(let i=0; i<selections.length; i++)
            {
                if(selections[i].get('status') === 'STOPPED')
                {
                    return true;
                }
            }

            return false;
        },  
        isSingleSelection: function(get)
        {
            let count = get('countSelected');
            
            return count === 1;
        },

        taskID: function (get) {
            let selTask = get('selections')[0];
            return selTask ? selTask.data.id : '';
        },
        taskName: function (get) {
            let selTask = get('selections')[0];
            return selTask ? selTask.data.name : '';
        },
        taskStartData: function (get) {
            let selTask = get('selections')[0];
            return selTask ? new Date(selTask.data.startData): '';
        },
        taskEndData: function (get) {
            let selTask = get('selections')[0];
            return selTask ? new Date(selTask.data.endData) : '';
        }
    },
 
}) 
      