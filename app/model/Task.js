Ext.define('App.model.Task', {
    extend: 'Ext.data.Model',
    identifier: 'sequential',
     
    fields: [ 
        {name: 'customId', type: 'int'},
        {name: 'name', type: 'string'},
        {name: 'startData', type: 'date'},
        {name: 'endData',  type: 'date'},
        {name: 'status',  type: 'string', defaultValue: 'STOPPED'}
    ],

    validators: { 
        name: { type: 'length', min: 1, max: 255 },
        startData:  'presence' ,
        endData: 'presence',
        status: { type: 'inclusion', list: ['RUNNING', 'STOPPED'] }
    }
});
