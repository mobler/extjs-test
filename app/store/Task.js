Ext.define('App.store.Task', {
    extend: 'Ext.data.Store',

    requires: [
        'App.model.Task' 
    ],
    model: 'App.model.Task',
    alias: 'store.task',
    storeId: 'task',

    data: []
});
