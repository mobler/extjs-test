Ext.define('App.viewController.TaskController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.task',

    onSelectionchange: function(btn, list)
    {
        this.getViewModel().set('selections', list);
        this.getViewModel().set('countSelected', list.length); 
    },

    onAddClick: function(){
        let rec = Ext.create('App.model.Task')
        let store = this.getViewModel().getStore('task');
        
        store.add(rec);
    },
    
    onRemoveClick: function(){
       let viewModel = this.getViewModel();
       let store = viewModel.getStore('task');
       let list =   viewModel.get('selections');
    
       for(let i=0; i<list.length; i++)
       {
            store.remove(list[i]);
       }   
     
    },

    onStopClick: function(){
        let viewModel = this.getViewModel();
        let list = viewModel.get('selections');
        
        
        for(let i=0; i<list.length; i++)
        { 
            list[i].set('status','STOPPED'); 
        }   
       
        // статус кнопок не обновляется
        // update viewModel data hack
        viewModel.set({selections:  []});
        viewModel.set({selections:  list});

     },

     onRunClick: function(){
        let viewModel = this.getViewModel();        
        let list = viewModel.get('selections');
       
        for(let i=0; i<list.length; i++)
        { 
           list[i].set('status','RUNNING'); 
        }   
        
        // статус кнопок не обновляется
        viewModel.set({selections:  []});
        viewModel.set({selections:  list});
     },

     onSumbitClick: function(){
       let viewModel = this.getViewModel();
       let store = viewModel.getStore('task');
       let rows = store.getRange();

       for(let i=0; i<rows.length; i++)
       {
           if(!rows[i].isValid())
           {
               return false;
           }
       }
       
       store.commitChanges();
     },

     onCancelClick: function(){
       let viewModel = this.getViewModel();
       let store = viewModel.getStore('task');

       store.rejectChanges();
    }
});
