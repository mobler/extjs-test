/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('App.Application', {
    extend: 'Ext.app.Application',

    name: 'App',

    // Add test records
    addData()
    {
        let store = Ext.getStore('task');

        store.add(Ext.create('App.model.Task',{ name: 'Task1',  startData: '10-01-2018 12:01:00', endData: '10-01-2018 12:01:00'}));
        store.add(Ext.create('App.model.Task',{ name: 'Task2',  startData: '10-01-2018 12:01:00', endData: '10-01-2018 12:01:00', status: 'RUNNING'}));
        store.add(Ext.create('App.model.Task',{ name: 'Task3',  startData: '10-01-2018 12:01:00', endData: '10-01-2018 12:01:00'}));

        store.commitChanges();
    },
    launch ()
    {  
        setTimeout(this.addData, 0);
    }
});
 